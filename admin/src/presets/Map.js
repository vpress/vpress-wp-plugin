export default {
    topojson: {
    url: '/topojsons/mx_edos.json',
    center: [ 19.6930200, -89.0503700 ],
    objects: ['states'],
    translate: undefined,
    zoom: 7,
    maxZoom: 10,
    minZoom: 5
  }
}