import install from './install'
import {
  extractDataType,
  queryByChart,
  mergeChartVal,
  queryBody,
  filterOrdinalParamsQuery,
  filterLinearParamsQuery
} from './helpers/index.js'

export default {
  install,
  extractDataType,
  queryByChart,
  mergeChartVal,
  queryBody,
  filterOrdinalParamsQuery,
  filterLinearParamsQuery
}
